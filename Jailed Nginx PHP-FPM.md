# Install oracle linux
In docker pull te oraclelinux:9 image. Notice: oraclelinux:latest was removed, so you must specify the version!
```
docker pull oraclelinux:9
```
Create a docker container from image:
```
docker run -it -d --privileged=true --name OracleLinuxNine -p 7845:80 oraclelinux:9 /sbin/init
```

# Install Nginx, PHP, PHP FPM and some tool
```
yum update
yum install nginx php php-fpm nano mc
systemctl start nginx
systemctl status nginx
systemctl start php-fpm
systemctl status php-fpm
```

# Create Environment variables to operate with scripts (jail direcotry path, website domain name, user for chroot)
```
JAILPATH=/jail
DOMAINNAME=apachetest.webflotta.hu
USER=apachetestwebflottahuuser
```

# Create jail directory structure
```
mkdir $JAILPATH
mkdir $JAILPATH/www
useradd -b $JAILPATH/www -k /dev/null -m $USER
mkdir $JAILPATH/www/$USER
mkdir $JAILPATH/www/$USER/chroot
mkdir $JAILPATH/www/$USER/chroot/data
mkdir $JAILPATH/www/$USER/chroot/log
mkdir $JAILPATH/www/$USER/chroot/tmp
mkdir $JAILPATH/www/$USER/chroot/tmp/misc
mkdir $JAILPATH/www/$USER/chroot/tmp/session
mkdir $JAILPATH/www/$USER/chroot/upload
mkdir $JAILPATH/www/$USER/chroot/wsdl
chown -R $USER:$USER $JAILPATH/www/$USER/chroot/
chmod 0010 $JAILPATH/www/$USER/chroot/
chmod 0070 $JAILPATH/www/$USER/chroot/data
chmod 0030 $JAILPATH/www/$USER/chroot/log
chmod 0010 $JAILPATH/www/$USER/chroot/tmp
chmod 0030 $JAILPATH/www/$USER/chroot/tmp/*
```

# Create separate PHP FPM config for website
```
touch /etc/php-fpm.d/$DOMAINNAME.conf
cat > /etc/php-fpm.d/$DOMAINNAME.conf << EOL
[${USER}]
user = $USER
group = $USER

listen = /var/run/php-fpm-$USER.sock
listen.owner = nginx
listen.group = nginx
 
pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 3

pm.status_path = /php-fpm-status
ping.path = /php-fpm-ping

access.format = "%t \"%m %r%Q%q\" %s %f %{mili}d %{kilo}M %C%%"
access.log = $JAILPATH/www/$USER/chroot/log/php-fpm-pool.log 
slowlog = $JAILPATH/www/$USER/chroot/log/php-fpm-slow.log
request_slowlog_timeout = 15s
request_terminate_timeout = 20s

chroot = $JAILPATH/www/$USER/chroot/
chdir = /

; Flags & limits
php_flag[display_errors] = off
php_admin_flag[log_errors] = on
php_admin_flag[expose_php] = off
php_admin_value[memory_limit] = 32M
php_admin_value[post_max_size] = 24M
php_admin_value[upload_max_filesize] = 20M
php_admin_value[cgi.fix_pathinfo] = 0
php_admin_value[disable_functions] = apache_child_terminate,apache_get_modules,apache_get_version,apache_getenv,apache_lookup_uri,apache_note,apache_request_headers,apache_reset_timeout,apache_response_headers,apache_setenv,getallheaders,virtual,chdir,chroot,exec,passthru,proc_close,proc_get_status,proc_nice,proc_open,proc_terminate,shell_exec,system,chgrp,chown,disk_free_space,disk_total_space,diskfreespace,filegroup,fileinode,fileowner,lchgrp,lchown,link,linkinfo,lstat,pclose,popen,readlink,symlink,umask,cli_get_process_title,cli_set_process_title,dl,gc_collect_cycles,gc_disable,gc_enable,get_current_user,getmygid,getmyinode,getmypid,getmyuid,php_ini_loaded_file,php_ini_scanned_files,php_logo_guid,php_sapi_name,php_uname,sys_get_temp_dir,zend_logo_guid,zend_thread_id,highlight_file,php_check_syntax,show_source,sys_getloadavg,closelog,define_syslog_variables,openlog,pfsockopen,syslog,nsapi_request_headers,nsapi_response_headers,nsapi_virtual,pcntl_alarm,pcntl_errno,pcntl_exec,pcntl_fork,pcntl_get_last_error,pcntl_getpriority,pcntl_setpriority,pcntl_signal_dispatch,pcntl_signal,pcntl_sigprocmask,pcntl_sigtimedwait,pcntl_sigwaitinfo,pcntl_strerror,pcntl_wait,pcntl_waitpid,pcntl_wexitstatus,pcntl_wifexited,pcntl_wifsignaled,pcntl_wifstopped,pcntl_wstopsig,pcntl_wtermsig,posix_access,posix_ctermid,posix_errno,posix_get_last_error,posix_getcwd,posix_getegid,posix_geteuid,posix_getgid,posix_getgrgid,posix_getgrnam,posix_getgroups,posix_getlogin,posix_getpgid,posix_getpgrp,posix_getpid,posix_getppid,posix_getpwnam,posix_getpwuid,posix_getrlimit,posix_getsid,posix_getuid,posix_initgroups,posix_isatty,posix_kill,posix_mkfifo,posix_mknod,posix_setegid,posix_seteuid,posix_setgid,posix_setpgid,posix_setsid,posix_setuid,posix_strerror,posix_times,posix_ttyname,posix_uname,setproctitle,setthreadtitle,shmop_close,shmop_delete,shmop_open,shmop_read,shmop_size,shmop_write,opcache_compile_file,opcache_get_configuration,opcache_get_status,opcache_invalidate,opcache_is_script_cached,opcache_reset

; Session
php_admin_value[session.entropy_length] = 1024
php_admin_value[session.cookie_httponly] = on
php_admin_value[session.hash_function] = sha512
php_admin_value[session.hash_bits_per_character] = 6
php_admin_value[session.gc_probability] = 1
php_admin_value[session.gc_divisor] = 1000
php_admin_value[session.gc_maxlifetime] = 1440

; Pathes
php_admin_value[include_path] = .
php_admin_value[open_basedir] = /data/:/tmp/misc/:/tmp/upload/:/dev/urandom
php_admin_value[sys_temp-dir] = /tmp/misc
php_admin_value[upload_tmp_dir] = /tmp/upload
php_admin_value[session.save_path] = /tmp/session
php_admin_value[soap.wsdl_cache_dir] = /tmp/wsdl
php_admin_value[sendmail_path] = /bin/sendmail -f -i
php_admin_value[session.entropy_file] = /dev/urandom
php_admin_value[openssl.capath] = /etc/ssl/certs

; I got always 403 Access denied from server...
; FastCGI sent in stderr: "Access to the script '/data' has been denied (see security.limit_extensions)"
security.limit_extensions = .php .php3 .php4 .php5 .html .htm
EOL
```

# Override Nginx server config
```
cat > /etc/nginx/nginx.conf << EOL
user nginx;
worker_processes auto;
pid /run/nginx.pid;

error_log /var/log/nginx/error.log debug;

events {
	worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
	disable_symlinks    on;
	server_tokens       off;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 4096;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;
    include /etc/nginx/conf.d/*.conf;
}
EOL
```

# Create separate Nginx config for website
```
touch /etc/nginx/conf.d/$DOMAINNAME.conf
cat > /etc/nginx/conf.d/$DOMAINNAME.conf << EOL
server {
    listen 80;
    # server_name $DOMAINNAME;

    access_log  /var/log/nginx/$DOMAINNAME.access.log;
    error_log /var/log/nginx/$DOMAINNAME.error.log debug;

    root $JAILPATH/www/$USER/chroot/data;
    index index.html index.htm index.php;

    location / {
        try_files $USERri $USERri/ /index.php$is_args$args;
    }

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php-fpm-$USER.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME /data$fastcgi_script_name;
        include /etc/nginx/fastcgi_params;
    }
}
EOL
```

# Create test PHP file
```
touch $JAILPATH/www/$USER/chroot/data/test.php
cat > $JAILPATH/www/$USER/chroot/data/test.php << EOL
<?php phpinfo(); ?>
EOL
```
```
chown $USER:$USER $JAILPATH/www/$USER/chroot/data/test.php
chmod 0640 $JAILPATH/www/$USER/chroot/data/test.php
usermod -a -G $USER nginx
```

# Create test HTML file
```
touch $JAILPATH/www/$USER/chroot/data/index.html
cat > $JAILPATH/www/$USER/chroot/data/index.html << EOL
Test from test.html
EOL
```

# Restart Nginx and PHP FPM
```
systemctl reload-or-restart nginx
systemctl restart php-fpm
```

# Troubleshoot
If not something not working check the following log files:
```
cat /var/log/nginx/error.log
cat /var/log/php-fpm/error.log
cat $JAILPATH/www/$USER/chroot/log/php-fpm-pool.log
```